COMPILER=clang++
INSTALL_CMD=install
SOURCE_DIR=./src

BUILD_DIR?=build
BIN_INSTALL_DIR?=/usr/bin
SYSTEMD_INSTALL_DIR?=/usr/lib/systemd/system

compile:
	mkdir -p ${BUILD_DIR}
	${COMPILER} ${SOURCE_DIR}/update_hosts.cpp -o${BUILD_DIR}/update_hosts
	${COMPILER} ${SOURCE_DIR}/to_hosts.cpp -ljsoncpp -o${BUILD_DIR}/to_hosts

install:
	${INSTALL_CMD} ${BUILD_DIR}/update_hosts ${BIN_INSTALL_DIR}/update_hosts
	${INSTALL_CMD} ${BUILD_DIR}/to_hosts ${BIN_INSTALL_DIR}/to_hosts
	${INSTALL_CMD} ${SOURCE_DIR}/docker_hosts_listener.sh ${BIN_INSTALL_DIR}/docker_hosts_listener
	${INSTALL_CMD} ./systemd/docker_hosts.service ${SYSTEMD_INSTALL_DIR}/docker_hosts.service

