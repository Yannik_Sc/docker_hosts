#include <stdio.h>
#include <iostream>
#include <json/reader.h>

int main() {
  Json::Value val;

  std::cin >> val;

  for(auto item : val) {
    try {
      std::string firstNetworkName = item["NetworkSettings"]["Networks"].getMemberNames()[0];
      std::string ip = item["NetworkSettings"]["Networks"][firstNetworkName]["IPAddress"].asString();

      if(ip.empty()) continue;

      std::cout << ip;
      std::cout << "\t";
    } catch(std::exception &e) {
      fprintf(stderr, "Could not map container %s", item["Name"].asCString());

      continue;
    }

    std::cout << item["Name"].asString().substr(1) + " ";
    std::cout << item["Id"].asString() + " ";
    std::cout << item["Id"].asString().substr(0, 12) + " ";
    std::cout << std::endl;
  }

  return 0;
}

