#!/usr/bin/bash

echo "initially updating hosts file"
docker container inspect $(docker ps -q) | to_hosts | sudo update_hosts;

echo Listening for docker events.;
docker events -f 'event=start' -f 'event=stop' -f 'event=die' | while read event; do
    echo Got event. Updating hosts.;
    docker container inspect $(docker ps -q) | to_hosts | sudo update_hosts;
done;

