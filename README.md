# docker_hosts

This collection of scripts will help you, to always be able to connect directly to your docker containers.
It listens for container start and stop events and updates the systems hosts file, by using docker and the provided applications.
**This application is only tested on Linux. It may support macOS. But it will not support Windows**


## Requirements
- docker

only for development:
- clang >= 5 (8 used in development. But 5 should be enough)
- json-cpp >= 1.8

## Usage
Build the applications and run the update service manually

```bash
# Build applications
make

# Install applications
sudo make install

# Start listener
sudo docker_hosts_listener
```

Run the application via systemd

```bash
# Reload the service unit (only required if the application just got installed or updated)
sudo systemctl daemon-reload

# Start the hosts update service
sudo systemctl start docker_hosts
```

### Make configuration
- `BUILD_DIR` The build directory. Built files will be outputted there (default: build)
- `BIN_INSTALL_DIR` The directory where the created executables should be installed in (default: /usr/bin)
- `SYSTEMD_INSTALL_DIR` The directory for the systemd unit file (default: /usr/lib/systemd/system)

